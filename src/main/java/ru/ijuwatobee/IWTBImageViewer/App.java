/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package ru.ijuwatobee.IWTBImageViewer;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class App extends Application {
    
    static final ResourceBundle bundle = PropertyResourceBundle.getBundle(Glossary.PROPERTIES_LABELS);
    
    public static void main(String[] args) {
        Application.launch(args);
    }
    
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        
        Node root = FXMLLoader.load(getClass().getResource("app.fxml"));
        Scene scene = new Scene((Parent) root, Color.DARKGREY);
        
        primaryStage.setTitle(bundle.getString(Glossary.PROPERTIES_APPNAME));
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    
}