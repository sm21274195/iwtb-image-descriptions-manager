package ru.ijuwatobee.IWTBImageViewer;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

public class Tab1Controller implements Initializable {
    @FXML TextField textField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }
    
    

    public Tab1Controller() {
        
    }
    
    
    
    @FXML
    protected void onClick() {
        System.out.println("Button click: " + textField.getText());
    }
}
