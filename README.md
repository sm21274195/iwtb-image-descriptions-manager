# IWTB Image Manager

Client application for managing Images' descriptions
(either remote and local ones).

*Current version:* 0.0.0

----

# Assembling and Running

### Required

- Java SDK 17 (or higher)
- JavaFX SDK 17 (or higher)
- Gradle 8.4

You can also use [Zulu JDK build](https://www.azul.com/) instead.

### How to assemble

```
cd <project_root_path>
gradle jar
```

### How to run

```
cd <project_root_path>
java -jar build/libs/iv-<current_version>.jar
```